import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int factorial = 1;
        try {
            System.out.print("Input an integer whose factorial will be computed: ");
            int numFactorial = scan.nextInt();
            for (int i = 1; i <= numFactorial; i++) {
                factorial *= i;
            }
            System.out.println("Factorial of " + numFactorial + " is " + factorial);
        } catch (InputMismatchException e) {
            System.out.println("Only integers are accepted for this system.");
        }

    }
}